import os
from flask import Flask


app = Flask(__name__)

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'data/dialogflow-agent-demo-e10995f912dd.json'
os.environ['DIALOGFLOW_PROJECT_ID'] = 'dialogflow-agent-demo'
os.environ['DIALOGFLOW_LANGUAGE_CODE'] = 'en'
os.environ['SESSION_ID'] = 'ja.msimango@gmail.com'

from app import routes
