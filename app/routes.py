import os
from app import app
from flask import request, make_response, jsonify
import dialogflow_v2 as dialogflow
from google.api_core.exceptions import InvalidArgument


DIALOGFLOW_PROJECT_ID = os.environ['DIALOGFLOW_PROJECT_ID']
DIALOGFLOW_LANGUAGE_CODE = os.environ['DIALOGFLOW_LANGUAGE_CODE']
SESSION_ID = os.environ['SESSION_ID']


@app.route('/')
def index():
    return 'Hello, User'


@app.route('/webhook', methods=['POST'])
def webhook():
    """ DialogFlow will process the text, then send back a fulfillment response """

    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(DIALOGFLOW_PROJECT_ID, SESSION_ID)

    try:
        if request.args.get('text'):
            text = request.args.get('text')
            text_input = dialogflow.types.TextInput(text=text, language_code=DIALOGFLOW_LANGUAGE_CODE)
            query_input = dialogflow.types.QueryInput(text=text_input)
    except UnboundLocalError:
        pass

    try:
        response = session_client.detect_intent(session=session, query_input=query_input)
    except InvalidArgument:
        raise

    print('Query text:', response.query_result.query_text)
    print('Detected intent:', response.query_result.intent.display_name)
    print('Fulfillment text:', response.query_result.fulfillment_text)

    return response.query_result.fulfillment_text
