import os

basedir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig(object):
    """ Base configuration """
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.urandom(24)


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    SESSION_COOKIE_SECURE = False


class ProductionConfig(BaseConfig):
    DEBUG = False
